package com.infosys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.infosys.entity.Product;
import com.infosys.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/product")
@Api(value = "/product", description = "Product configuration", produces = "application/json")
public class ProductController {

	@Autowired
	private ProductService productService;

	@ApiOperation(value = "Add Product", response = Product.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product added successfully", response = Product.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> addProduct(@RequestBody Product product) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("message", "product added successully");
		productService.addProduct(product);
		return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Retrieve all Products", response = Product.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product Retrieved successfully", response = Product.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProducts() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("message", "Product Retrieved successfully");
		List<Product> products = productService.getAllProduct();
		return new ResponseEntity<>(products, httpHeaders, HttpStatus.OK);
	}

	@ApiOperation(value = "Retrieve Product by Id", response = Product.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retrieve Product by Id", response = Product.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Product> getProductById(@PathVariable("id") int id) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("message", "product info ");
		Product product = productService.getProductById(id);
		return new ResponseEntity<>(product, httpHeaders, HttpStatus.OK);
	}

	@ApiOperation(value = "Update Product", response = Product.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Update Product", response = Product.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<HttpHeaders> updateProduct(@RequestBody Product product) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("message", "product updated successully ");
		productService.updateProduct(product);
		return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete Product", response = Product.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Delete Product", response = Product.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<HttpHeaders> deleteProductById(@PathVariable("id") int id) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("message", "product deleted successully ");
		productService.deleteProductById(id);
		return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
	}

}
