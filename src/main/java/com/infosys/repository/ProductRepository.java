package com.infosys.repository;

import org.springframework.data.repository.CrudRepository;

import com.infosys.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{

}
