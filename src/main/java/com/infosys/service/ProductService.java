package com.infosys.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infosys.entity.Product;
import com.infosys.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;

	public void addProduct(Product product) {
		productRepository.save(product);
	}
	
	public List<Product> getAllProduct() {
		List<Product> products = new ArrayList<>();
		productRepository.findAll().forEach(products::add);
		return products;
		
	}

	public void updateProduct(Product product) {
		Product getProduct = productRepository.findById(product.getProductId()).get();
		if( getProduct == null ) {
			throw new NullPointerException("Product not found");
		}
		productRepository.save(product);
	}

	public void deleteProductById(int productId) {
		productRepository.deleteById(productId);
	}

	public Product getProductById(int id) {
		return productRepository.findById(id).get();
	}


}
